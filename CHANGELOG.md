# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]

### Added
- Nothing significant in this release

### Changed
- Bugfix, users can now set their title on their membership [!92](https://gitlab.com/worldcon/2020-wellington/merge_requests/92)

### Removed
- Nothing significant in this release



## [Tag 1.4.1 - 2019-07-22]

Hotpatch, in 1.4.0 we regressed the payments mailer for instalments which no longer send. This patch release fixes that
mailer.

### Added
- Nothing significant in this release

### Changed
- Fixed regression, instalments mailer now sends happily [!91](https://gitlab.com/worldcon/2020-wellington/merge_requests/91)

### Removed
- Nothing significant in this release


## [Tag 1.4.0 - 2019-07-22]

This release has a bit of everything. We're making life better for other cons with support for multi currency, our
support staff who have more fatures for adjusting memberships, and our developers have better seeded data for something
that feels better right out of the box.

### Added
- Added brakeman, ruby-audit, and bundler-audit vulnerability scanners to the build process and `make test`
  [!85](https://gitlab.com/worldcon/2020-wellington/merge_requests/85)
- Configurable currency, add STRIPE_CURRENCY to your .env and all prices are now in that currency
  [!70](https://gitlab.com/worldcon/2020-wellington/merge_requests/70)
- Support can set membership to any level, including past memberships
  [!87](https://gitlab.com/worldcon/2020-wellington/merge_requests/87)
- Support can credit memberships with cash, allows support to create and credit memberships
  [!87](https://gitlab.com/worldcon/2020-wellington/merge_requests/87)
- User notes are now exposed on the reservation show screen
  [!87](https://gitlab.com/worldcon/2020-wellington/merge_requests/87)
- Upgrades and membership changes are now shown to our members
  [!87](https://gitlab.com/worldcon/2020-wellington/merge_requests/87)

### Changed
- Added unique constraint to membership number data model
  [!70](https://gitlab.com/worldcon/2020-wellington/merge_requests/70). Please check and correct duplicates with this:
  ```
  Reservation.having("count(membership_number) > 1").group(:membership_number).pluck(:membership_number)
  ```
- Upgraded gems to the latest versions [!83](https://gitlab.com/worldcon/2020-wellington/merge_requests/83)
- Generated memberships in testing now have charges
  [!84](https://gitlab.com/worldcon/2020-wellington/merge_requests/84)
  and [!86](https://gitlab.com/worldcon/2020-wellington/merge_requests/86)
- Support can now transfer memberships that are in instalment
  [!88](https://gitlab.com/worldcon/2020-wellington/merge_requests/88)

### Removed
- Nothing significant in this release


## [Tag 1.3.0 - 2019-06-19]

### Added
- Purchase flow changed to let you select a membership before signing in [!73](https://gitlab.com/worldcon/2020-wellington/merge_requests/73)
- Prominant prices, membership rights and buttons on all memberships [!73](https://gitlab.com/worldcon/2020-wellington/merge_requests/73)

### Changed
- Upgraded gems to the latest versions [!76](https://gitlab.com/worldcon/2020-wellington/merge_requests/76)
- Renamed Purchase to Reservation to match the domain more closely
- Fixed charge descriptions in Stripe and in Charge comments
  [!80](https://gitlab.com/worldcon/2020-wellington/merge_requests/80).
  Cleanup retrospectively with this rake task post release:
  ```bash
  bundle exec rake stripe:sync:charges
  ```

### Removed
- Paths to resources have changed, /purchases have moved to /reservations


## Tag 1.2.0 - 2019-06-08

Upgraded Ruby and Rails, and support function for transferring memberships.

### Added
- Turned on [Content Security Policy (CSP)](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy)
  for the site for security hygiene
- Support can now transfer memberships between users from a user's detail form

### Changed
- Moved task `check:models` to `test:models` to keep namespaces tight
- Run rails upgrades for 5.2 so we gret the most out of our setup
- Upgrade Ruby from 2.5.1 to 2.5.3
- Update gems to the latest versions
- Bugfix on support list, now transferred memberships show user's details correctly
- Order displayed membership offers by price, highest to lowest
- Fixed a bug where you could upgrade Adult to Adult memberships after the price increase

### Removed
- Nothing significant in this release


## Tag 1.1.0 - 2019-05-22

Some quality of life improvements for support, and general cleanup with things we learnt from our initial release.

### Added
- Detection of Stripe test keys to change colours on pages to distinguish between production and test systems
- Migrations to correct data corruption on imported timestamps
- Customer stripe ID now recorded and reused from User model going forward
- New rake tasks for your utility belt:
  ```bash
  # Copy over stripe customer details to users with
  bundle exec rake stripe:sync:customers

  # Update historical charge descriptions in stripe and charge comments with
  bundle exec rake stripe:sync:charges

  # Detect invalid records on your systems with
  bundle exec rake test:models
  ```
- CoNZealand images are now served from the project rather than GitHub to consolidate infrastructure
- When purchasing a new membership, if you've got existing memberships you now get linked to the 'Review Memberships'
  section with a helpful message
- Added Policy and Terms of service to CoNZealand pages

### Changed
- URLs for charging a person have been updated to use Purchase for consistency
- Fixed Kansa and Presupport import methods to set "active" correctly on older records
- Charge descriptions in stripe now describe amount owed, type of payment, membership name and number
- Allow database name to be configurable on production builds for cheep staging costs
- Updated most gems in the project including Rails
- Replaced deprecated SASS gem with SASSC
- Redirect to current page on login, puts you on the "new membership" or "review memberships" pages
- New styles added to support page for readability
- Email address added to support page for findability
- Performance improvements for support memberships listing
- Makefile has smarter build targets that create databases and images as needed
- Developer setup steps in README should run out of the box

### Removed
- Nothing significant in this release


## Tag 1.0.0 - 2019-03-29

Initial release of CoNZealand, intended to give people what they had with Kansa, introduce instalments and bring in our
pre supporters.

### Added
- Basic forms for purchasing memberships based on CoNZealand paper forms
- Payments, including pay by instalment
- Upgrades between different membership types
- Login through email links that last 10 minutes
- Basic support area that lists memberships
- Concept of "active" for membership pricing for price rotation and disabling memberships
- Concept of "active" for membership held and claim over membership
- Concept of "active" for claim over membership for transfers and history of ownership
- Theme concept area so we may cater for different cons
- Basic mailers to setup descriptions about payment
- Basic docker images for developers and production
- Gitlab CI pipelines that build docker images for deploy
- Command line based membership transfer
- Membership numbers start at 100 to give room for special guests

### Changed
- Kansa members were renumbered to start at 2000
- Old Kansa login links now say "this link has expired"

### Removed
- Nothing significant in this release
